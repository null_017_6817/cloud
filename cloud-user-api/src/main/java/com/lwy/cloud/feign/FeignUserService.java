package com.lwy.cloud.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Component
@FeignClient(value = "user-lwy")
public interface FeignUserService {

    @RequestMapping(value = "/test/feign" , method = RequestMethod.GET)
    public String testFeign();

}
