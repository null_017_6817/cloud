package com.lwy.cloud.module.user.service.impl;

import com.lwy.cloud.feign.FeignUserService;
import com.lwy.cloud.module.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    FeignUserService feignUserService;

    public String testFeignCallUser(){
        return  feignUserService.testFeign();
    }

}
