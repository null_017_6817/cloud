package com.lwy.cloud.module.user.controller;


import com.lwy.cloud.module.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {


    @Autowired
    UserService userService;

    @RequestMapping(value = "test/feign/call/user" , method = RequestMethod.GET)
    public String testFeignCallUser(){

        return  userService.testFeignCallUser();
    }


}
