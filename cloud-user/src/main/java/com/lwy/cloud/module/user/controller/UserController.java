package com.lwy.cloud.module.user.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    /**
     * @Author liwenyong
     * @Description 测试通过feign调用
     * @Date 2021/8/10
     * @Param []
     * @return java.lang.String
     **/
    @RequestMapping(value = "/test/feign" , method = RequestMethod.GET)
    public String testFeign(){
        return "Successfully call user service through feign";
    }



}
